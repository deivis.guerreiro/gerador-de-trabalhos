const fs = require('fs');
const state = require('./state.js')
let PptxGenJS = require("pptxgenjs");



function robot() {
  const content = state.load();
  let pptx = new PptxGenJS();
  pptx.layout = 'LAYOUT_WIDE';
  pptx.defineSlideMaster({
    title: 'PLACEHOLDER_SLIDE',
    bkgd: 'FFFFFF',
    objects: [{
        'rect': {
          x: 0,
          y: 0,
          w: '100%',
          h: 0.75,
          fill: 'F1F1F1'
        }
      },
      {
        'text': {
          text: `${content.prefix} ${content.searchTerm}`,
          options: {
            x: 1,
            y: 0,
            w: 6,
            h: 0.75
          }
        }
      },
      {
        'placeholder': {
          options: {
            name: 'body',
            type: 'body',
            x: 0.6,
            y: 1.5,
            w: 12,
            h: 5.25
          },
          text: '(custom placeholder text!)'
        }
      },
      {
        'image': {
          x: 11.3,
          y: 6.40,
          w: 1.67,
          h: 0.75,
          path: 'download.jpeg'
        }
      }
    ],
    slideNumber: {
      x: 0.3,
      y: '95%'
    }
  });
  pptx.defineSlideMaster({
    title: 'TITLE_SLIDE',
    bkgd: {
      path: 'download.jpeg'
    },
    objects: [{
      'rect': {
        x: 0,
        y: 1.5,
        w: '100%',
        h: 0.75,
        fill: 'F1F1F1'
      },
      'text': {
        text: `${content.prefix} ${content.searchTerm}`,
        options: {
          x: 0.6,
          y: 1.5,
          w: 12,
          h: 12 
        }
      },
      'placeholder': {
        options: {
          name: 'body',
          type: 'body',
          x: 0.6,
          y: 1.5,
          w: 12,
          h: 5.25
        },
        text: '(custom placeholder text!)'
      }
    }],
    slideNumber: {
      x: 0.3,
      y: '95%'
    }
  });
  var slidetitle = pptx.addSlide('TITLE_SLIDE');

  for (const sentence of content.sentences) {
    var slide = pptx.addSlide('PLACEHOLDER_SLIDE');
    let topics = [];
    for (let topic = 0; topic < sentence.keywords.length / 2; topic++) {
      topics.push({
        text: sentence.keywords[topic],
        options: {
          bullet: true,
          color: '000000'
        }
      })
    }

    slide.addText(
      topics, {
        placeholder: 'body'
      }
    );
    // slide.addText(sentence.text, { placeholder:'body' });
  }
  pptx.writeFile('Sample Presentation');
  console.log("Slide Pronto");

}

module.exports = robot