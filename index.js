const readline = require('readline-sync')
const robots = {
    input: require('./bots/input.js'),
    text: require('./bots/text.js'),
    state: require('./bots/state.js'),
    slide: require('./bots/slide.js')
}

async function start() {
    robots.input()
    await robots.text()
    await robots.slide()
    //console.log(robots.state.load());
}

start()